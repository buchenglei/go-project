package test

import (
	"testing"
)

func TestHelloCover(t *testing.T) {
	result := HelloCover(0)
	if result != "Hello Mike" {
		t.Errorf("error: want 'Hello Mike', get '%s'", result)
	}

	t.Run("test name 'Job'", func(t *testing.T) {
		result := HelloCover(1)
		if result != "Hello Job" {
			t.Errorf("error: want 'Hello Job', get '%s'", result)
		}
	})

	t.Run("test name 'Bob'", func(t *testing.T) {
		result := HelloCover(3)
		if result != "Hello Bob" {
			t.Errorf("error: want 'Hello Bob', get '%s'", result)
		}
	})
}
