package test

import "fmt"

func HelloCover(cond int) string {
	name := "unknown"
	switch cond {
	case 0:
		name = "Mike"
	case 1:
		name = "Job"
	case 2:
		name = "Alice"
	case 3:
		name = "Bob"
	}

	return fmt.Sprintf("Hello %s", name)
}
