package test

import "testing"

func Add(nums ...int) int {
	result := 0
	for _, n := range nums {
		result += n
	}

	return result
}

func TestAdd(t *testing.T) {
	result := Add(0, 1)
	if result != 1 {
		t.Errorf("error: want '1', get '%d'", result)
	}

	t.Run("test exp '1 + 1'", func(t *testing.T) {
		result := Add(1, 1)
		if result != 2 {
			t.Errorf("error: want '2', get '%d'", result)
		}
	})

	t.Run("test exp '3 + 1'", func(t *testing.T) {
		result := Add(3, 1)
		if result != 4 {
			t.Errorf("error: want '4', get '%d'", result)
		}
	})
}

// 这使用一种更加优雅和有效便编写测试用例的方式

// define input-result struct type
type TestDataItem struct {
	inputs   []int // inputs to `Add` function
	result   int   // result of `Add` function
	hasError bool  // does `Add` function returns error
}
